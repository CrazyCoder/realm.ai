from realm_ai.core.actor import Actor

class TestActor(Actor):
           
    def __init__(self, id):
        super(TestActor, self).__init__(id)
        self.environmentUpdateCalls = 0
    
    def onEnvironmentUpdate(self, realmView):
        self.environmentUpdateCalls += 1
        self.receivedRealm = realmView
        
