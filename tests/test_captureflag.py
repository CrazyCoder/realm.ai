from unittest import TestCase

from realm_ai.captureflag.realm import Realm
from tests.testActor import TestActor
import math

class TestCaptureFlag(TestCase):
    def testThatRealmIsSentToActorOnRegistration(self):
        s = Realm()
        actor = TestActor("id1")
        s.AddActor(actor)
        self.assertEqual(1, actor.environmentUpdateCalls)
        
    def testThatRealmActorViewHasReducedVisibility(self):
        s = Realm()
        actor = TestActor("id1")
        
        s.AddActor(actor)
        receivedRealm = actor.receivedRealm
        self.assertIsNotNone(receivedRealm)
        self.assertEqual(0, len(receivedRealm.blocks))
        self.assertEqual(0, len(receivedRealm.actors))
        
    def testThatActorPositionIsUpdatedWhenItMoves(self):
        s = Realm()
        actor1 = TestActor("id1")
        s.AddActor(actor1)
        self.assertEqual(5, actor1.position[0])
        self.assertEqual(5, actor1.position[1])
        s.Move(actor1, 0)
        self.assertEqual(6, actor1.position[0])
        self.assertEqual(5, actor1.position[1])
        
    def testThatActorSeesBlockWhenItMovesEnough(self):
        s = Realm()
        actor1 = TestActor("id1")
        s.AddActor(actor1)
        s.Move(actor1, math.pi/4)
        s.Move(actor1, math.pi/4)
        s.Move(actor1, math.pi/4)
        s.Move(actor1, math.pi/4)
        self.assertEqual(7.828427124746192, actor1.position[0])
        self.assertEqual(7.828427124746192, actor1.position[1])
        receivedRealm = actor1.receivedRealm
        self.assertEqual(1, len(receivedRealm.blocks))
        
    def testThatActorCatchTheFlagAndOtherActorIsNotified(self):
        s = Realm()
        actor1 = TestActor("id1")
        s.AddActor(actor1)
        actor2 = TestActor("id2")
        s.AddActor(actor2)
        for i in range(0, 20):
            s.Move(actor1, math.pi/2)
            
        for i in range(0, 10):
            s.Move(actor1, 0)
        
        actor1ReceivedRealm = actor1.receivedRealm
        actor2ReceivedRealm = actor2.receivedRealm
        self.assertEqual(31, actor2.environmentUpdateCalls)
        self.assertEqual(15, actor1.position[0])
        self.assertEqual(25, actor1.position[1])
        self.assertEqual("id1", actor1ReceivedRealm.flagHolder.id)
        self.assertEqual("id1", actor2ReceivedRealm.flagHolder.id, msg = actor2ReceivedRealm)
        