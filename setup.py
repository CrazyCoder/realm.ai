from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="realm_ai", 
    version="0.1", 
    url="https://gitlab.com/CrazyCoder/realm.ai", 
    author="CrazyCoder", 
    license="MIT", 
    long_description=long_description,
    packages=["realm_ai"],
    test_suite='nose.collector',
    tests_require=['nose'])