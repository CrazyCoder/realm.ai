from realm_ai.captureflag.actorView import ActorView
import math

REALM_SIZE = 30
ACTOR_SPEED = 1

class Realm:
    
    def __init__(self):
        self.size = [REALM_SIZE, REALM_SIZE]
        self.flagPosition = []
        self.actors = []
        self.size = []
        self.blocks = []
        self.flagHolder = None
        self._initRealm()
        
    def _initRealm(self):
        self.flagPosition = [REALM_SIZE / 2, REALM_SIZE * 5 / 6]
        block1 = (REALM_SIZE / 2, REALM_SIZE * 1 / 6)
        self.blocks.append(block1)
        block2 = (REALM_SIZE / 3, REALM_SIZE * 2 / 6)
        self.blocks.append(block2)
        block3 = (REALM_SIZE * 2 / 3, REALM_SIZE * 2 / 6)
        self.blocks.append(block3)
        block4 = (REALM_SIZE / 2, REALM_SIZE / 2)
        self.blocks.append(block4)
        block5 = (REALM_SIZE / 3, REALM_SIZE * 4 / 6)
        self.blocks.append(block5)
        block6 = (REALM_SIZE * 2 / 3, REALM_SIZE * 4 / 6)
        self.blocks.append(block6)
        
    def _initActor(self, actor):
        if(len(self.actors) == 0):
            actor.position = [REALM_SIZE / 6, REALM_SIZE / 6]
        
        if(len(self.actors) == 1):
            actor.position = [REALM_SIZE * 5 / 6, REALM_SIZE / 6]
        
        self.actors.append(actor)
        
    def AddActor(self, actor):
        self._initActor(actor)
        actor.onEnvironmentUpdate(ActorView(self, actor))
        
    def Move(self, actor, direction):
        actor.position[0] = actor.position[0] + math.cos(direction) * ACTOR_SPEED
        actor.position[1] = actor.position[1] + math.sin(direction) * ACTOR_SPEED
        
        if(math.isclose(actor.position[0], self.flagPosition[0], rel_tol=0.1) and math.isclose(actor.position[1], self.flagPosition[1], rel_tol=0.1)):
            self.flagHolder = actor
        
        for i in self.actors:
            i.onEnvironmentUpdate(ActorView(self, i))