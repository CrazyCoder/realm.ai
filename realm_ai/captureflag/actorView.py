import math

REALM_SIGHT = 5

class ActorView:
     
    def __init__(self, realm, actor):
        self.flagPosition = []
        self.actors = []
        self.blocks = []
        self.flagHolder = realm.flagHolder
        self.realm = realm
        
        for block in realm.blocks:
            if(self.dist(actor.position, block) < REALM_SIGHT):
                self.blocks.append(block)
        
        for actorItem in realm.actors:
            if(self.dist(actor.position, actorItem.position) < REALM_SIGHT and actorItem.id != actor.id):
                self.actors.append(actorItem)
    
    def Move(self, actor, direction):
        self.realm.Move(actor, direction)

    def dist(self, pos1, pos2):
        return math.sqrt((pos2[0] - pos1[0])**2 + (pos2[1] - pos1[1])**2)
